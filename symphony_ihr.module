<?php

/**
 * IHR Events Symphony itegration
 * This module adds some IHR website specific blocks and pages.
 *
 * To use it create a node for each seminar and give the node a URL alias.
 * Add any title and seminar description text you like.
 * Go to Configuration > Web Services > Symphony API and add a key|value to the
 * Seminar URL mapping. The Key corresponds to the URL alias of the node, and the
 * Label corresponds to the Symphony API 'series' string field search value.
 */


/**
 * Implements HOOK_form_FORM_ID_alter() to inject custom IHR Events module admin settings.
 * We use the list.module functions to store an array variable for the seminar map.
 */
function symphony_ihr_form_symphony_admin_form_alter(&$form, &$form_state, $form_id) {
  $form['symphony_ihr_seminar_map'] = array(
    '#type' => 'textarea',
    '#multiple' => FALSE,
    '#title' => t('Seminar URL mapping'),
    '#description' => t('The possible seminar names/values this field can contain. Enter one value per line, in the format key|label.<br />
The key is the full URL alias of the node. The label will be used in displayed values and is the specific case sensitive string value in the Symphony \'series\' field.<br />
The label is optional: if a line contains a single string, it will be used as key and label.<br />
If the Seminar series is not present in the Seminar URL mapping it will not be exposed as a seminar page.'),
    '#default_value' => list_allowed_values_string(variable_get('symphony_ihr_seminar_map')),
  );

  // Add custom submit handler to save map as an array
  $form['#submit'][] = 'symphony_ihr_symphony_admin_form_submit_handler';
}
function symphony_ihr_symphony_admin_form_submit_handler($form, &$form_state) {
  $map = list_extract_allowed_values($form['symphony_ihr_seminar_map']['#value'], 'list_text', FALSE);
  variable_set('symphony_ihr_seminar_map', $map);
}


/**
* Implementation of hook_menu.
* These replace our old ihr_sas_events.module menu routes
* with 410 Gone headers.
*/
function symphony_ihr_menu() {
  $items['events/browse/%'] = array(
    'title' => t('History events at the IHR'),
    'page callback' => '_symphony_ihr_events_moved_message',
    'page arguments' => array(2), // Pass the matched wildcard (3nd URL var) as a param to the callback.
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );
  $items['events/ihr'] = array(
    'title' => t('History events at the IHR'),
    'page callback' => '_symphony_ihr_events_moved_message',
    'page arguments' => array(2), // Should be the year/month in YYYY-MM str format.
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  // New IHR weekly diary - i.e. IHR Events browse and search
  $items['events/diary'] = array(
    'title' => t('IHR diary'),
    'page callback' => 'symphony_ihr_events_ihr_diary',
    'access callback' => TRUE,
    'type' => MENU_CALLBACK,
  );

  return $items;
}


/**
 * Returns the 410 Gone IHR Events moved message, telling users
 * and search engines to go elsewhere.
 */
function _symphony_ihr_events_moved_message($param = '') {
  if (!empty($param) && !is_numeric($param)) {
    return MENU_NOT_FOUND;
  }

  // Else, it's probably an old event id.
  drupal_add_http_header('Status', '410 Gone', FALSE);
  drupal_set_title(t('410 Gone - IHR Events have moved a bit'));
  return t('We have updated our events system and this has changed our Event URL\'s.<br />
    Please visit main !events-section to find current events.',
    array('!events-section' => l('IHR Events section', 'events')));
}


/**
 * The IHR module wraps the symphonyapi.module events block in a page menu
 * to display under our events/diary url. This function loads and alters the
 * block a little before rendering to the page.
 */
function symphony_ihr_events_ihr_diary() {
  $title = t('IHR diary');
  drupal_set_title($title);

  // Load block (in a few steps).
  $block = block_load('symphony', 'symphony_events');
  $block_content = _block_render_blocks(array($block));
  $block_content['symphony_symphony_events']->subject = '';  // Remove block title
  $build = _block_get_renderable_array($block_content);

  return drupal_render($build);
}

/**
 * Preprocess templates a bit differently.
 */
function symphony_ihr_preprocess_symphony_results(&$vars) {
  foreach ($vars['results'] as &$result) {
    if(isset($result['booking_form_button'])) {
      $result['booking_form_button']['#options']['attributes']['class'] = 'btn btn-info action-link';
    }
    if(isset($result['image_thumb'])) {
      $result['image_thumb']['#width'] = 120;
    }
    $result['start_date_formatted'] = format_date($result['StartDateUnix'], 'custom', 'l, j F, g:ia');

    // Generate some custom values
    $result['speakers'] = _symphony_get_speakers_for_event($result['EventId']);
    $result['display_location'] = _generate_location_string($result);
  }
}
function symphony_ihr_preprocess_symphony_event_display_page(&$vars) {
  if(isset($vars['event']['booking_form_button'])) {
    $vars['event']['booking_form_button']['#options']['attributes']['class'] = 'btn btn-info action-link';
  }

  // Generate some custom values
  $vars['event']['speakers'] = _symphony_get_speakers_for_event($vars['event']['EventId']);
  $vars['event']['display_location'] = _generate_location_string($vars['event']);

  // Highlight the IHR events menu item on event display pages
  menu_tree_set_path('main-menu', 'events/diary');
}
function symphony_ihr_preprocess_symphony_results_list(&$vars) {
  foreach ($vars['results'] as $eid => &$result) {
    $result['start_date_formatted'] = format_date($result['StartDateUnix'], 'custom', 'l, j F, g:ia');

    // Generate some custom values
    $result['speakers'] = _symphony_get_speakers_for_event($result['EventId']);
    $result['display_location'] = _generate_location_string($result);
  }
}


/**
 * Implements hook_theme() and hook_theme_registry_alter()
 * to hijack/override some template files.
 *
 * TODO: make the IHR website conform to the SAS events module templates
 * and remove these overrides!
 */
function symphony_ihr_theme() {
  return array(
    'symphony_api_filter_form' => array(
      'render element' => 'form',
      'arguments' => array('form' => NULL),
      'template' => 'symphony-api-filter-form',
      'path' => drupal_get_path('module', 'symphony_ihr') . '/theme',
    ),
   'symphony_ihr_seminar_table' => array(
      'arguments' => array('results' => NULL),
      'template' => 'symphony-ihr-seminar-table',
      'path' => drupal_get_path('module', 'symphony_ihr') . '/theme',
    ),
  );
}
function symphony_ihr_theme_registry_alter(&$theme_registry) {
  if (isset($theme_registry['symphony_results_list'])) {
    $new_path = drupal_get_path('module', 'symphony_ihr') . '/theme';
    $theme_registry['symphony_results_list']['path'] = $new_path;
  }
  if (isset($theme_registry['symphony_results'])) {
    $new_path = drupal_get_path('module', 'symphony_ihr') . '/theme';
    $theme_registry['symphony_results']['path'] = $new_path;
  }
}


/**
 * Implements custom HOOK_series_link_alter()
 * Alters an event's series_link path injecting the specific URL from the
 * module's 'Symphony URL mapping' key|value configuration.
 */
function symphony_ihr_series_link_alter(&$link, $event) {
  if (!empty($link['#options']['query']['series'])) {
    $map = array_flip(variable_get('symphony_ihr_seminar_map'));
    $series = $link['#options']['query']['series'];
    if(array_key_exists($series, $map)) {
      $link['#path'] = $map[$series];
      unset($link['#options']['query']);
      unset($link['#options']['attributes']['target']);  // Open in same window on IHR site
      show($link);
    }
  }
}


/**
 * IHR Seminars pages
 * Page view hook callback to inject a seminar's events into a seminar page.
 */
function symphony_ihr_node_view($node, $view_mode, $langcode) {

  // Check for valid seminar
  $seminar_alias = request_path();
  $map = variable_get('symphony_ihr_seminar_map');
  if (!array_key_exists($seminar_alias, $map)) {
    return;  // Bail if no seminar key matches config.
  }

  $series = $map[$seminar_alias];

  // Setup search params
  $instituteid = variable_get('symphony_default_client_company_id', 0);
  $start_date_search = __get_start_of_term();
  $page_number = 0; // 0 returns all results.

  $results = _symphony_get_results($instituteid, $start_date_search, NULL, NULL, NULL, NULL, $page_number, NULL, NULL, $series);
  $numresults = $results['numresults'];
  $events = $results['events'];

  // Add some custom fields
  foreach ($events as &$event) {
    $event['start_date_formatted'] = format_date($event['StartDateUnix'], 'custom', 'j F');
    $event['start_time_formatted'] = format_date($event['StartDateUnix'], 'custom', 'H:i');
    $event['academic_term'] = __get_term_tag($event['StartDateUnix'], $event['EndDateUnix']);
    $event['speakers'] = _symphony_get_speakers_for_event($event['EventId']);
    $event['display_location'] = _generate_location_string($event);
  }

  // Add results to the node content for rendering.
  $node->content['seminar_table'] = array(
    '#type' => 'markup',
    '#markup' => theme('symphony_ihr_seminar_table', array('results' => $events)),
    '#weight' => 10,
  );
}


/**
 * Function to determine the Academic Term title from a start and end timestamp
 * Term date ranges used:
 * - Autumn Term, 01 Sept-31 Dec
 * - Spring Term, 01 Jan-31 Mar
 * - Summer Term, 01 Apr-30 Jun
 *
 * @param  timestamp $time_start Event start time
 * @param  timestamp $time_end   Event end time
 * @return string                Title string of the term
 */
function __get_term_tag($time_start, $time_end) {
  // Determine year from input
  $year = date('Y', $time_start);
  // Generate term timestamps
  $autumn_start = strtotime('01 Sept ' . $year);
  $autumn_end = strtotime('31 Dec ' . $year);
  $spring_start = strtotime('01 Jan ' . $year);
  $spring_end = strtotime('31 Mar ' . $year);
  $summer_start = strtotime('01 Apr ' . $year);
  $summer_end = strtotime('30 Jun ' . $year);

  // Calculate term tag of input
  if (($autumn_start <= $time_start) && ($time_end <= $autumn_end)) {
    return t('Autumn Term !year', array('!year' => $year));
  }
  if (($spring_start <= $time_start) && ($time_end <= $spring_end)) {
    return t('Spring Term !year', array('!year' => $year));
  }
  if (($summer_start <= $time_start) && ($time_end <= $summer_end)) {
    return t('Summer Term !year', array('!year' => $year));
  }
  return '';
}


/**
 * Function to calculate the timestamp for the start of a term relative
 * to the input time (defaults to now).
 *
 * Term date ranges used:
 * - Spring Term, 01 Jan-31 Mar
 * - Summer Term, 01 Apr-30 Jun
 * - Autumn Term, 01 Sept-31 Dec
 *
 * @param  timestamp $time Timestamps, defaults to time()
 * @return timestamp       Timestamp for the start the closest academic term.
 */
function __get_start_of_term($time = NULL) {
  if ($time == NULL) {
    $time = time();
  }

  $year = date('Y', $time);  // Determine year from input

  // Generate term timestamps (plus/minus 1 term each side of current year)
  $summer_start_prev = strtotime('01 Apr ' . ($year - 1));
  $summer_end_prev = strtotime('30 Jun ' . ($year - 1));
  $autumn_start_prev = strtotime('01 Sept ' . ($year - 1));
  $autumn_end_prev = strtotime('31 Dec ' . ($year - 1));

  $spring_start = strtotime('01 Jan ' . $year);
  $spring_end = strtotime('31 Mar ' . $year);
  $summer_start = strtotime('01 Apr ' . $year);
  $summer_end = strtotime('30 Jun ' . $year);
  $autumn_start = strtotime('01 Sept ' . $year);
  $autumn_end = strtotime('31 Dec ' . $year);

  $spring_start_next = strtotime('01 Jan ' . ($year + 1));

  // Calculate relative term start time
  if (($autumn_end_prev < $time) && ($time <= $spring_end)) {
    return $spring_start;
    // return t('Spring Term !year', array('!year' => $year));  // Debugging
  }
  if (($spring_end < $time) && ($time <= $summer_end)) {
    return $summer_start;
    // return t('Summer Term !year', array('!year' => $year));  // Debugging
  }
  if (($summer_end < $time) && ($time <= $autumn_end)) {
    return $autumn_start;
    // return t('Autumn Term !year', array('!year' => $year));  // Debugging
  }
  if (($summer_end_prev < $time) && ($time <= $autumn_end)) {
    return $autumn_start_prev;
    // return t('Autumn Term !year', array('!year' => $year - 1));  // Debugging
  }
  if ($autumn_end < $time) {
    return $spring_start_next;
    // return t('Spring Term !year', array('!year' => $year + 1));  // Debugging
  }

  return $time;  // Return input time otherwise
}

/**
 * Internal function to return a Room/Location string from
 * a symphony event array object.
 * The Symphony Room Codes contain 2 values:
 * - Value 1 is used in the Room dropdown selection menu and
 * - Value 2 is auto populated into the Location field.
 *
 * The issue is the nomenclature/formatting of the values.
 * Value 2 is often too long to use for display,
 * but if we display Value 1 we lose the capacity to override
 * or customise the location.
 *
 * This function looks at the Code_Room and the Location fields
 * and if ', Senate House' exists in the Location field, we
 * return everything BEFORE that, otherwise, we return the entire
 * Location field. If Location is empty, return Code_Room
 *
 * Currently only used in IHR templates.
 *
 * @param  array $event Symphony event array object
 * @return string The shortened/modified Room/Location string.
 */
function _generate_location_string($event) {
  if (empty($event['Location'])) {
    return $event['Code_Room'];
  }

  $delim = ', Senate House';
  $pieces = explode($delim, $event['Location']);
  if (count($pieces) > 0) {
    return $pieces[0];
  }

  return $event['Location'];
}