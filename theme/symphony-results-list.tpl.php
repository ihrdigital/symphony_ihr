<?php
/**
 * @file
 * Template file for the symphony table results display.
 *
 * Variables available:
 * $results: The results array (the SOAP response transformed into an array)
 *
 * The table header is always needed, even if empty, for the pagination plugin
 *
 */
// dpm($results);
?>
<?php if (count($results)==0): ?>
  <div class="views-row">
    <article class="node node-event node-teaser">
      <div class="content">No events found</div>
    </article>
  </div>
<?php else: ?>
  <?php foreach ($results as $result): ?>
    <div class="views-row">
      <article class="node node-event node-teaser">
        <header>
          <div class="details">
          <?php if(!empty($result['start_date_formatted'])): ?>
            <div class="date"><?php print $result['start_date_formatted']; ?></div>
          <?php endif; ?>
          </div>
          <h1><?php print drupal_render($result['event_display_link']); ?></h1>
          <?php if (!empty($result['display_location'])): ?>
            <span class="location"><?php print $result['display_location']; ?></span>
          <?php endif; ?>
        </header>
      </article>
    </div>
  <?php endforeach; ?>
<?php endif; ?>
