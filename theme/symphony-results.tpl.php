<?php
/**
 * @file
 * Template file for the symphony table results display.
 *
 * Variables available:
 * $results: The results array (the SOAP response transformed into an array)
 *
 * The table header is always needed, even if empty, for the pagination plugin
 *
 */
?>
<div id="sym-eventslist">
  <?php if (count($results) == 0): ?>
    <p><?php print t('No events found.'); ?></p>
  <?php else: ?>
    <?php foreach ($results as $result): ?>
      <div class="sym-event-row">
        <div class="sym-event-image">
          <?php if (!empty($result['image_thumb'])): ?>
            <?php print render($result['image_thumb']); ?>
          <?php endif; ?>
        </div>

        <div class="sym-event-details">
          <strong class='sym-event-name sym-event-item'>
            <?php print render($result['event_display_link']); ?>
          </strong>

          <?php if (!empty($result['speakers'])): ?>
            <div class="sym-event-item eventSpeakers">
            <?php foreach ($result['speakers'] as $delta => $speaker): ?>
              <?php if (isset($speaker['speaker_formatted'])): ?>
                <?php print $speaker['speaker_formatted']; ?><?php if ($delta < (count($result['speakers']) - 1)): ?>,<?php endif; ?>
              <?php endif; ?>
            <?php endforeach; ?>
            </div>
          <?php endif; ?>

          <div class="sym-event-desc sym-event-item">
            <?php print $result['ResultDescription']; ?>
          </div>

          <div class="sym-event-dets sym-event-item">
            <?php if (!empty($result['institute_link']) && !$result['institute_link']['#printed']): ?>
              <?php print drupal_render($result['institute_link']); ?><br />
            <?php endif; ?>
            <?php if (!empty($result['series_link']) && !$result['series_link']['#printed']): ?>
              <?php print drupal_render($result['series_link']); ?><br />
            <?php endif; ?>
            <?php if (!empty($result['Code_Type'])): ?>
              <?php print $result['Code_Type']; ?><br />
            <?php endif; ?>
            <?php if (!empty($result['StartDateUnix'])): ?>
              <span class="date"><?php print $result['start_date_formatted']; ?></span><br />
            <?php endif; ?>
            <?php if (!empty($result['display_location'])): ?>
              <?php print $result['display_location']; ?><br />
            <?php endif; ?>
          </div>

          <?php if (!empty($result['booking_form_button'])): ?>
            <div class="sym-event-book sym-event-item">
              <?php print drupal_render($result['booking_form_button']); ?>
            </div>
          <?php endif; ?>
        </div>
      </div>
    <?php endforeach; ?>
  <?php endif; ?>
</div>
