<?php
/**
 * @file
 * Template for the symphony filter form.
 */
  // Unfortunately I have to remove some of the custom SAS theme markup
  // to use the IHRCMS omega theme grid classes.
  unset($form['event_name']['#prefix']);
  unset($form['subinstitute_id']['#prefix']);
  unset($form['subinstitute_id']['#suffix']);
  unset($form['aoi']['#prefix']);
  unset($form['aoi']['#suffix']);
  unset($form['event_name']['#prefix']);
  unset($form['type']['#prefix']);
  unset($form['end_date']['#suffix']);
  $form['submit']['#attributes'] = array('class' => array('btn btn-info action-link'));
  hide($form['institute_id']);
?>
<div class="grid-10 alpha omega box1 box3" style="margin-bottom: 20px;">
  <div class="content pane-content">

    <div class="grid-3 alpha">
      <?php print drupal_render($form['event_name']); ?>
      <?php print drupal_render($form['subinstitute_id']); ?>
    </div>
      <div class="grid-3">
      <?php print drupal_render($form['type']); ?>
      <?php print drupal_render($form['start_date']); ?>
    </div>
    <div class="grid-3 omega">
      <?php print drupal_render($form['aoi']); ?>
      <?php print drupal_render($form['end_date']); ?>
    </div>
    <?php print drupal_render($form['submit']); ?>
  </div>
</div>
<?php print drupal_render_children($form); ?>