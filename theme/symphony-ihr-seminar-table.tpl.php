<?php
/**
 * @file
 * Template file for the symphony IHR Seminar results page.
 *
 * Variables available:
 * $results: The results array (the SOAP response transformed into an array)
 *
 * Each event should be tagged with it's academic_term. Each of these creates a
 * separate term table.
 *
 */
// Group events by term
$term_events = array();
foreach ($results as $result) {
  $term_events[$result['academic_term']][] = $result;
}
?>
<?php if (count($results)==0): ?>
  <div class="desc">
    <p>Seminar programme to be announced.</p>
  </div>
<?php else: ?>
  <?php foreach ($term_events as $term => $events): ?>
    <table class="table table-condensed table-bordered widgetEvent">
      <caption><?php print $term; ?></caption>
      <thead>
        <tr><td width="100px;">Date</td><td>Seminar details</td>
      </tr></thead>
      <tbody>
        <?php foreach ($events as $event): ?>
          <tr class="nobord">
            <td class="date">
              <?php print $event['start_date_formatted']; ?><br />
              <?php print $event['start_time_formatted']; ?>
            </td>
            <td>
              <span class="eventName"><strong><?php print drupal_render($event['event_display_link']); ?></strong></span><br>
              <?php if (!empty($event['speakers'])): ?>
              <span class="eventSpeakers">
                <?php foreach ($event['speakers'] as $delta => $speaker): ?>
                  <?php if (isset($speaker['speaker_formatted'])): ?>
<?php print $speaker['speaker_formatted']; ?><?php if ($delta < (count($event['speakers']) - 1)): ?>,<?php endif; ?>
                  <?php endif; ?>
                <?php endforeach; ?>
              </span><br />
              <?php endif; ?>
              <?php if (!empty($event['display_location'])): ?>
                <div class="eventInfo">
                <?php print $event['display_location']; ?>
                </div><br />
              <?php endif; ?>
            </td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  <?php endforeach; ?>
<?php endif; ?>